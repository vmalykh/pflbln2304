package com.pflb.education.java.equality;

public class EqualityTest {
  public static void main(String[] args) {
    int a1 = 2;
    long b1 = 2;
    System.out.println(a1 == b1);

    Integer a2 = 2;
    Long b2 = 2L;
    System.out.println(a2.equals(b2));

    Integer a3 = 2;
    long b3 = 2;
    System.out.println(a3.equals(b3));

    Integer a4 = 2000;
    long b4 = 2000;
    System.out.println(a4 == b4);

    Long a5 = 2000L;
    Long b5 = 2000L;
    System.out.println(a5 == b5);

    Long a6 = 2000L;
    Long b6 = 2000L;
    System.out.println(a6 == b6);

    long a7 = a6;
    long b7 = b6;
    System.out.println(a7 == b7);
  }
}
