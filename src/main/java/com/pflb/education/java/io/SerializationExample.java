package com.pflb.education.java.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationExample {
  public static final String savePath = "/tmp/vasya.bin";

  public static void main(String[] args) throws IOException, ClassNotFoundException {
//    User vasya = new User("vasya", "vasya@mail.ru", 245);
//    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(savePath));
//    oos.writeObject(vasya);

    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(savePath));
    User user = (User) ois.readObject();
    System.out.println(user);
  }
}

class User implements Serializable {
  private String name;
  private String email;
  private int age;

  public User(String name, String email, int age) {
    this.name = name;
    this.email = email;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public int getAge() {
    return age;
  }

  @Override
  public String toString() {
    return "User {" +
        "name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", age=" + age +
        '}';
  }
}
