package com.pflb.education.java.io;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class IOExample {
    public static void unbufferedRead() {
        FileInputStream inputStream = null;
        try {
            int ch; //char
            while ((ch = inputStream.read()) != -1) {
                System.out.print((char)ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                inputStream.close();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public static void bufferedRead() {
        StringBuilder sb = new StringBuilder();
        try (BufferedInputStream is = new BufferedInputStream(
                new FileInputStream("/tmp/big.txt")
        )) {
            byte[] buffer = new byte[100];
            while (is.available() != 0) {
                is.read(buffer);
                sb.append(new String(buffer, StandardCharsets.UTF_8));
            }
            System.out.println(sb);
        } catch (IOException e) {
            e.printStackTrace();
        }


//        String strToWrite = "Hello world!";
//        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("/tmp/newtestfile.txt"))) {
//            outputStream.write(strToWrite.getBytes(StandardCharsets.UTF_8));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    public static void main(String[] args) {
        bufferedRead();
    }
}
