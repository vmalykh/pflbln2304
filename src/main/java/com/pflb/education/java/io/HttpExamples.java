package com.pflb.education.java.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpExamples {
  public static final String BASIC_URL = "https://swapi.dev/api/";

  public static void javaHttpUrlConnectionExample() throws IOException {
    URL url = URI.create(BASIC_URL).resolve("people/1/").toURL();
    URLConnection urlConnection = url.openConnection();
    HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
    httpURLConnection.setRequestMethod("GET");
    httpURLConnection.setInstanceFollowRedirects(false);

//    urlConnection.addRequestProperty("Content-Type", "application/json");
    httpURLConnection.addRequestProperty("Accept", "application/json");
    httpURLConnection.setDoOutput(true);
//    OutputStream out = urlConnection.getOutputStream();
//    out.write("{\"abc\":\"zxc\"}".getBytes(StandardCharsets.UTF_8));
//    out.flush();
//    out.close();
    InputStream in = httpURLConnection.getInputStream();
    byte[] responseBytes = in.readAllBytes();
    String responseText = new String(responseBytes, StandardCharsets.UTF_8);
    System.out.println(responseText);
  }

  public void javaHttpClientExample() throws IOException, InterruptedException {
    URI uri = URI.create(BASIC_URL).resolve("starships/2/");
    BodyPublisher bodyPublisher = BodyPublishers.ofString("{\"abc\":\"zxc\"", StandardCharsets.UTF_8);
    Builder builder = HttpRequest.newBuilder(uri)
        .setHeader("Accept", "application/json")
//        .POST(bodyPublisher)
        .GET();
    HttpRequest request = builder.build();

    HttpClient.Builder clientBuilder = HttpClient.newBuilder();
    BodyHandler<String> bodyHandler = BodyHandlers.ofString(StandardCharsets.UTF_8);
    CookieManager cookieManager = new CookieManager(new InMemoryCookieStore(),
        CookiePolicy.ACCEPT_ALL);
    HttpClient client = clientBuilder.cookieHandler(cookieManager).build();

    HttpResponse<String> response = client.send(request, bodyHandler);
    System.out.printf("Status: %d\n%s", response.statusCode(), response.body());
  }

  public static void main(String[] args) throws IOException {
    javaHttpUrlConnectionExample();
  }

}


class InMemoryCookieStore implements CookieStore {
  private Map<URI, List<HttpCookie>> store;


  @Override
  public void add(URI uri, HttpCookie cookie) {
    List<HttpCookie> list = store.computeIfAbsent(uri, k -> new ArrayList<>());
    list.add(cookie);
  }

  @Override
  public List<HttpCookie> get(URI uri) {
    List<HttpCookie> list = store.get(uri);
    return list == null ? new ArrayList<>() : list;
  }

  @Override
  public List<HttpCookie> getCookies() {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public List<URI> getURIs() {
    return store.keySet().stream().toList();
  }

  @Override
  public boolean remove(URI uri, HttpCookie cookie) {
    List<HttpCookie> list = store.get(uri);
    if (list == null) return false;
    if (!list.contains(cookie)) return false;
    list.remove(cookie);
    return true;
  }

  @Override
  public boolean removeAll() {
    store = new HashMap<>();
    return true;
  }
}