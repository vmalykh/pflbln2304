package com.pflb.education.java.oop;

public class Circle extends AbstractShape {
  public static final double PI = 3.14;
  private double radius;

  public Circle(double radius) {
    this.radius = radius;
  }

  @Override
  public double getPerimeter() {
    return PI * 2 * radius;
  }

  @Override
  public double getSquare() {
    return radius * radius * PI;
  }
}
