package com.pflb.education.java.oop;

public class Square extends Rectangle {
  public Square(double side) {
    super(side, side);
  }
}
