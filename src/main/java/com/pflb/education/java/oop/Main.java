package com.pflb.education.java.oop;

public class Main {
  public static void main(String[] args) {
    Circle circle1 = new Circle(4);
    Circle circle2 = new Circle(6);

    System.out.println(circle1.getPerimeter());
    System.out.println(circle1.getSquare());
    System.out.println(circle2.getPerimeter());
    System.out.println(circle2.getSquare());

    System.out.println("Rectangles:");

    Rectangle rectangle1 = new Rectangle(2, 4);
    Rectangle rectangle2 = new Rectangle(3, 8);

    System.out.println(rectangle1.getPerimeter());
    System.out.println(rectangle1.getSquare());
    System.out.println(rectangle2.getPerimeter());
    System.out.println(rectangle2.getSquare());

    Square square1 = new Square(5);
    Square square2 = new Square(6);

    AbstractShape[] shapes = {circle1, circle2, rectangle1, rectangle2, square1, square2};

    System.out.println("From loop");
    for (AbstractShape shape : shapes) {
      System.out.println(shape.toString());
    }
  }

  public void printGeom(Geom obj) {
    System.out.println("Perimeter is: " + obj.getPerimeter());
    System.out.println("Square is: " + obj.getSquare());
  }
}
