package com.pflb.education.java.oop;

public class Rectangle extends AbstractShape {
  private double sideA;
  private double sideB;

  public Rectangle(double sideA, double sideB) {
    this.sideA = sideA;
    this.sideB = sideB;
  }

  @Override
  public double getPerimeter() {
    return 2 * sideA + 2 * sideB;
  }

  @Override
  public double getSquare() {
    return sideA * sideB;
  }

}
