package com.pflb.education.java.oop;

public interface Geom {
  double getPerimeter();
  double getSquare();
}
