package com.pflb.education.java.oop;

public abstract class AbstractShape implements Geom {

  @Override
  public String toString() {
    return "Shape type: %s \nperimeter: %s\nsquare: %s\n\n".formatted(getClass().getSimpleName(),
        getPerimeter(), getSquare());
  }
}
