package com.pflb.education.java.functional;

@FunctionalInterface
public interface CarFactory<T extends Car> {
    T create(String name);
}