package com.pflb.education.java.functional;

public class Car {
    private String name;

    public Car(String name) {
        this.name = name;
        System.out.println("Создан Car с именем " + name);
    }

    public static void collide(Car car) {
        System.out.println("Collided " + car);
    }

    public void follow(Car anotherCar) {
        System.out.println(this.name + " Following the " + anotherCar.name);
    }

    public void repair() {
        System.out.println("Repaired " + this.name);
    }

    @Override
    public String toString() {
        return "Car with name " + this.name;
    }
}

