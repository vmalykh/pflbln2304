package com.pflb.education.java.functional;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        CarFactory<Car> cf = Car::new; //ссылка на метод конструктора
        CarFactory<Car> c1 = n -> new Car(n);
        CarFactory<Car> c2 = new CarFactory<Car>() {
            @Override
            public Car create(String name) {
                return new Car(name);
            }
        };
        CarFactory<Car> c3 = new CarFactoryImpl();


        //создадим пару машин
        Car car1 = cf.create("car1");
        Car car2 = cf.create("car2");


        List<Car> carList = Arrays.asList(car1, car2); //создадим лист из машин

        carList.forEach(Car::repair); //Ссылка на метод произвольного экземпляра класса
        carList.forEach(car -> car.repair());

        carList.forEach(car2::follow); //Ссылка на метод конкретного объекта
        carList.forEach(car -> car2.follow(car));

        carList.forEach(Car::collide);
        carList.forEach(car -> Car.collide(car));

        carList.forEach(System.out::println);
    }
}

class CarFactoryImpl implements CarFactory<Car> {
    @Override
    public Car create(String name) {
        return new Car(name);
    }
}