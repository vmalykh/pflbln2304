package com.pflb.education.java.patterns;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

interface ResultHandler<T> {
  T handle(String context);
}

public class Executor<T> {
  public T processFile(String filename, ResultHandler<T> handler) {
    try (FileInputStream inputStream = new FileInputStream(filename)) {
      byte[] bytes = inputStream.readAllBytes();
      String fileContent = new String(bytes, StandardCharsets.UTF_8);
      T result = handler.handle(fileContent);
      return result;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void main(String[] args) {
    Executor<String> executor = new Executor<>();
    executor.processFile("/tmp/big.txt", content -> {
      System.out.println(content);
      return "OK";
    });
  }

}
