package com.pflb.education.java.databases.domain;

import com.pflb.education.java.databases.annotations.Column;
import com.pflb.education.java.databases.annotations.Table;

@Table("city")
public class City {
  @Column("ID")
  public int id;

  @Column("Name")
  public String name;

  @Column("CountryCode")
  public String countryCode;

  @Column("Population")
  public int population;

  @Override
  public String toString() {
    return "City{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", countryCode='" + countryCode + '\'' +
        ", population=" + population +
        '}';
  }
}
