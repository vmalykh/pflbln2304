package com.pflb.education.java.databases.dao;

import com.pflb.education.java.databases.ConnectionManager;
import com.pflb.education.java.databases.annotations.Column;
import com.pflb.education.java.databases.annotations.Table;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoUtils {
  private DaoUtils() {}

  public static <T> List<T> getList(Class<T> type) {
    List<T> resultList = new ArrayList<>();
    String tableName = getTableName(type);
    Connection connection = ConnectionManager.getConnection();
    Statement statement = null;
    try {
      statement = connection.createStatement();
      String sql = "SELECT * FROM `" + tableName + "`";
      ResultSet resultSet = statement.executeQuery(sql);
      while (resultSet.next()) {
        T instance = createNewInstance(type);
        resultList.add(instance);
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
          Class<?> fieldType = field.getType();
          String columnName = getColumnName(field);
          Object value;
          if (fieldType.equals(int.class)) {
            value = resultSet.getInt(columnName);
          } else if (fieldType.equals(String.class)) {
            value = resultSet.getString(columnName);
          } else {
            throw new RuntimeException("Тип не поддерживается");
          }
          setInstanceValue(instance, field, value);
        }
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    finally {
      try {
        if (statement != null) {
          statement.close();
        }
        connection.close();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    }
    return resultList;
  }

  public static <T> T getById(int id, Class<T> type) {
    String tableName = getTableName(type);
    Connection connection = ConnectionManager.getConnection();
    PreparedStatement statement = null;
    String sql = "SELECT * FROM `" + tableName + "` WHERE id = ?";
    try {
      statement = connection.prepareStatement(sql);
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      if (!resultSet.next()) throw new RuntimeException(String.format("Запись с id %d не найдена", id));
      T instance = createNewInstance(type);
      Field[] fields = type.getDeclaredFields();
      for (Field field : fields) {
        Class<?> fieldType = field.getType();
        String columnName = getColumnName(field);
        Object value;
        if (fieldType.equals(int.class)) {
          value = resultSet.getInt(columnName);
        } else if (fieldType.equals(String.class)) {
          value = resultSet.getString(columnName);
        } else {
          throw new RuntimeException("Тип не поддерживается");
        }
        setInstanceValue(instance, field, value);
      }
      return instance;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private static String getTableName(Class<?> type) {
    Table annotation = type.getDeclaredAnnotation(Table.class);
    String value = annotation.value();
    return value;
  }

  private static <T> T createNewInstance(Class<T> type) {
    try {
      Constructor<T> constructor = type.getDeclaredConstructor();
      return constructor.newInstance();
    } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
             InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  private static String getColumnName(Field field) {
    field.setAccessible(true);
    Column annotation = field.getDeclaredAnnotation(Column.class);
    String value = annotation.value();
    return value;
  }

  public static void setInstanceValue(Object instance, Field field, Object value) {
    try {
      field.set(instance, value);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
