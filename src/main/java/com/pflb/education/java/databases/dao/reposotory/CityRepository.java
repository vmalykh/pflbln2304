package com.pflb.education.java.databases.dao.reposotory;

import com.pflb.education.java.databases.dao.DaoUtils;
import com.pflb.education.java.databases.domain.City;
import java.util.List;

public class CityRepository implements JdbcRepository<City> {

  @Override
  public Class<City> getParametrizedType() {
    return City.class;
  }

  @Override
  public City getById(int id) {
    return DaoUtils.getById(id, getParametrizedType());
  }

  @Override
  public List<City> getList() {
    return DaoUtils.getList(getParametrizedType());
  }
}
