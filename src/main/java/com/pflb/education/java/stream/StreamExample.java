package com.pflb.education.java.stream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {
    public static void main(String[] args) {
        //Попытка найти "ресурс" и получить как объект типа File
        try (BufferedInputStream is = new BufferedInputStream(
                new FileInputStream("D:\\big.txt")
        )) {
            //Читаем файл и сохраняем в одну большую строку
            byte[] buffer = new byte[1024];
            StringBuilder sb = new StringBuilder();
            while (is.available() != 0) {
                is.read(buffer);
                sb.append(new String(buffer));
            }

            //разиваем строку по пробельным символам и создаем "Стрим"
            Stream<String> stream = Stream.of(sb.toString().split(" "));

            String words = stream.filter(e -> e.startsWith("e")) //Отфильтровать, оставить слова начинающиеся на "e"
                    .limit(500) //Применить максимальный лимит 500
                    .map(s -> s.replace("\n", ""))
                     //.count(); //Терминальная операция. Посчитаем кол-во оставщихся объектов
                     //.forEach(System.out::println); //еще пример термиальной операци. Вывести каждый элемент в консоль
                    .collect(Collectors.joining(", "));//Еще пример терминальной операции. "Собрать" все элементы в строку, с раздеителем - запятой
//            System.out.println(words);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
