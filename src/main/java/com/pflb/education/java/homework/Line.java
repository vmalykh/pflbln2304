package com.pflb.education.java.homework;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Класс отрезков на 2-мерной координатной плоскости.
 */
public class Line {
  private Point start;
  private Point end;

  public Line(int startCoordinateX, int startCoordinateY, int endCoordinateX, int endCoordinateY) {
//    this.start = new Point(startCoordinateX, startCoordinateY);
//    this.end = new Point(endCoordinateX, endCoordinateY);
    this(new Point(startCoordinateX, startCoordinateY), new Point(endCoordinateX, endCoordinateY));
  }

  public Line(Point start, Point end) {
    this.start = start;
    this.end = end;
    validate();
  }

  private void validate() {
    if (start.x == end.y && start.y == end.y) {
//      throw new RuntimeException("Координаты старта и конца отрезка совпадают");
      System.out.println("Координаты старта и конца отрезка совпадают");
    }
  }

  /**
   * Check given lines length with current.
   * @param anotherLine line to compare
   * @return true if the lengths are equals
   */
  public boolean checkLength(Line anotherLine) {
    return anotherLine.length() == this.length();
  }

  /**
   * Calculate length.
   * @return length value
   */
  public double length() {
    double sum = Math.pow(end.x - start.x, 2)
            + Math.pow(end.y - start.y, 2);
    return Math.sqrt(sum);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Line)) return false;
    Line anotherLine = (Line) obj;
    return start.x == anotherLine.start.x &&
        end.x == anotherLine.end.x &&
        start.y == anotherLine.start.y &&
        end.y == anotherLine.end.y;
  }

  @Override
  public int hashCode() {
    return start.x + start.y
        + end.x + end.y;
  }

  /**
   * Class represents point on 2-dimension coordinate surface.
   */
  private final static class Point {
    public final int x;
    public final int y;

    public Point(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }

  @Override
  public String toString() {
    return String.format("Line [%d, %d] to [%d, %d]", start.x, start.y,
        end.x, end.y);
  }

  public static void main(String[] args) {
    Line line1 = new Line(1, 2, 2, 2);
    Line line2 = new Line(-3, 0, 1, 1);

    Point start1 = new Point(1, 2);
    Point end1 = new Point(2, 2);
    Line line3 = new Line(start1, end1);

    if (line1.equals(line3)) {
      System.out.println("Линии 1 и 3 равны!");
    } else {
      System.out.println("Линии 1 и 3 не равны!");
    }

    if (line1.checkLength(line2)) {
      System.out.println("Длины линий 1 и 3 равны");
    } else {
      System.out.println("Длины линий 1 и 3 не равны");
    }

    System.out.println(line1.hashCode());
    System.out.println(line2.hashCode());
    System.out.println(line3.hashCode());

    HashSet<Line> lines = new HashSet<>();
    lines.add(line1);
    lines.add(line2);
    lines.add(line3);
    System.out.println(lines);

    HashMap<Line, String> map = new HashMap<>();
    map.put(line1, "Линия первая");
    map.put(line2, "Линия вторая");
    String previous = map.put(line3, "Линия третья");

    System.out.println(map.get(line1));
    System.out.println(map.get(line2));
    System.out.println(map.get(line3));
    System.out.println(previous);
  }
}
