package com.pflb.education.java.homework.labels;

public enum Label {
  SPAM, NEGATIVE_TEXT, TOO_LONG, OK
}
