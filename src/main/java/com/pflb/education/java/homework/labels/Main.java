package com.pflb.education.java.homework.labels;

public class Main {
  public static void main(String[] args) {
    String positiveTextToProcess = "Lorem ipsum dolor";
    String negativeTextToProcess = "Lorem ipsum dolor :(";
    String longText = "Lorem ipsum dolor Lorem ipsum dolor";
    String spanText = "Lorem ipsum dolor spam1, spam2";

    NegativeTextAnalyzer negativeTextAnalyzer = new NegativeTextAnalyzer();
    SpamTextAnalyzer spamTextAnalyzer = new SpamTextAnalyzer("spam1", "spam2");
    TooLongTextAnalyzer tooLongTextAnalyzer = new TooLongTextAnalyzer(10);
    TextAnalyzer[] analyzers = {negativeTextAnalyzer, spamTextAnalyzer, tooLongTextAnalyzer};

    for (TextAnalyzer analyzer : analyzers) {
      Label label = analyzer.processText(positiveTextToProcess);
      assert label.equals(Label.OK) : String.format("Label should ne OK, but actual, %s", label);
      if (!label.equals(Label.OK)) {
        throw new RuntimeException(String.format("Label should be OK, but actual: %s", label));
      }
    }
  }

  public static Label processText(TextAnalyzer[] analyzers, String textToProcess) {
    for (TextAnalyzer analyzer : analyzers) {
      Label label = analyzer.processText(textToProcess);
      if (!label.equals(Label.OK)) return label;
    }
    return Label.OK;
  }
}
