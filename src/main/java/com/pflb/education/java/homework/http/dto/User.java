package com.pflb.education.java.homework.http.dto;

public class User {
  public int id;
  public String firstName;
  public String secondName;
  public int money;
  public int age;
  public Sex sex;

  public enum Sex {
    MALE, FEMALE
  }
}
