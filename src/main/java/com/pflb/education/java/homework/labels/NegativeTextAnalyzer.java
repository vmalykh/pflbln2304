package com.pflb.education.java.homework.labels;

public class NegativeTextAnalyzer extends AbstractTextAnalyzer {
  private static final String[] NEGATIVE_SMILES = {":(", "=(", ":|"};

//  public NegativeTextAnalyzer() {
////    super(NEGATIVE_SMILES);
//    this.keywords = NEGATIVE_SMILES;
//  }

  @Override
  public String[] getKeywords() {
    return NEGATIVE_SMILES;
  }

  @Override
  public Label getLabel() {
    return Label.NEGATIVE_TEXT;
  }
}
