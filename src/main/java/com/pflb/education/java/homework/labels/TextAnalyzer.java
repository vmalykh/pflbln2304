package com.pflb.education.java.homework.labels;

public interface TextAnalyzer {
  Label processText(String text);
}
