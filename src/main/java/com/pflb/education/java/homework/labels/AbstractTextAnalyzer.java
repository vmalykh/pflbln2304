package com.pflb.education.java.homework.labels;

public abstract class AbstractTextAnalyzer implements TextAnalyzer {
//  protected String[] keywords;
//
//  public AbstractTextAnalyzer(String... keywords) {
//    this.keywords = keywords;
//  }

  public abstract String[] getKeywords();

  public abstract Label getLabel();

  @Override
  public Label processText(String text) {
    for (String keyword : getKeywords()) {
      if (text.contains(keyword)) return getLabel();
    }
    return Label.OK;
  }
}
