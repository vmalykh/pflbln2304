package com.pflb.education.java.homework.labels;

public class SpamTextAnalyzer extends AbstractTextAnalyzer {
  private String[] keywords;

  public SpamTextAnalyzer(String... keywords) {
    this.keywords = keywords;
  }

  @Override
  public String[] getKeywords() {
    return keywords;
  }

  @Override
  public Label getLabel() {
    return Label.SPAM;
  }
}
