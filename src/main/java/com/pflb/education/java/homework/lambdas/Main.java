package com.pflb.education.java.homework.lambdas;

public class Main {
  public static double step = 0.1;

  public static void main(String[] args) {
    double result = integrate(1, 10, x -> 1);
    System.out.println(result);
  }

  static double integrate(double start, double end, Function function) {
    double result = 0;
    for (double x = start; x < end; x += step) {
      double rectArea = function.getValue(x) * step;
      result += rectArea;
    }
    return result;
  }
}

interface Function {
  double getValue(double x);
}

