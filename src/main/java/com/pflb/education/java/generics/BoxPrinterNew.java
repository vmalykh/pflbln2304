package com.pflb.education.java.generics;

public class BoxPrinterNew<T> {
    private T var;

    public BoxPrinterNew(T obj) {
        this.var = obj;
    }

    @Override
    public String toString() {
        return ("{ " + var + " }");
    }

    public T getVar() {
        return var;
    }
}
