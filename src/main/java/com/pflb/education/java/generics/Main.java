package com.pflb.education.java.generics;

public class Main {
    public static void main(String[] args) {
        BoxPrinter bOld1 = new BoxPrinter(5);
        BoxPrinter bOld2 = new BoxPrinter("test");
        BoxPrinterNew<Integer> bNew1 = new BoxPrinterNew<>(5);
        BoxPrinterNew<String> bNew2 = new BoxPrinterNew<>("test1");

        Integer oldVal1 = (Integer)bOld1.getVar();
        String oldVal2 = (String)bOld1.getVar();

        Integer newVal1 = bNew1.getVar();
//        String newVal2 = (String) bNew1.getVar();
    }
}
