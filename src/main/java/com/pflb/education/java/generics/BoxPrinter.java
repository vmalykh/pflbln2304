package com.pflb.education.java.generics;

public class BoxPrinter {
    private Object var;

    public BoxPrinter(Object obj) {
        this.var = obj;
    }

    @Override
    public String toString() {
        return ("{ " + var + " }");
    }

    public Object getVar() {
        return var;
    }
}