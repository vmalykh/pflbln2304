package com.pflb.education.kt.geom

class Square(side: Double) : Rectangle(side, side)