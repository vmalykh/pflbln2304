package com.pflb.education.kt.geom

class Circle constructor(var radius: Double) : Geom {
    override val perimeter: Double
        get() = Math.PI * radius * radius

    override val square: Double
        get() = Math.PI * 2 * radius
}