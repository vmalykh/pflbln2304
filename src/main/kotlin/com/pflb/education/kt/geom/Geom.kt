package com.pflb.education.kt.geom

interface Geom {
    val square: Double
    val perimeter: Double
}