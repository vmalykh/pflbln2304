import com.pflb.education.kt.geom.Circle
import com.pflb.education.kt.geom.Geom
import com.pflb.education.kt.geom.Rectangle
import com.pflb.education.kt.geom.Square

fun main() {
    val circle = Circle(3.4)
    val rectangle = Rectangle(2.0, 5.toDouble())
    val square = Square(2.3)

    val shapes = listOf<Geom>(circle, rectangle, square)
    for (shape in shapes) {
        println(shape)
    }

    shapes.forEach(::println)

}

