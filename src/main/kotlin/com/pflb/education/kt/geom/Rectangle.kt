package com.pflb.education.kt.geom

open class Rectangle(sideA: Double, sideB: Double) : Geom {
    private val sideA: Double
    private val sideB: Double

    init {
        this.sideA = sideA
        this.sideB = sideB
    }

    override val perimeter: Double
        get() = sideA * sideB

    override val square: Double
        get() = 2 * sideA + 2 * sideB

}