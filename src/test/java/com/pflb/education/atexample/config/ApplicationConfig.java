package com.pflb.education.atexample.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationConfig {
  public final String env;
  public final String baseUrl;
  public final String apiUrl;
  public final String username;
  public final String userPassword;
  public final String hubUrl;

  private static final String ENV_NAME = "ACTIVE_ENVIRONMENT";
  private final Properties properties = parseProperties();

  public ApplicationConfig() {
    String getenv = System.getenv(ENV_NAME);
    if (getenv == null) env = "default";
    else env = getenv;

    this.baseUrl = getProperty("base.url");
    this.apiUrl = getProperty("api.url");
    this.username = getProperty("user.login");
    this.userPassword = getProperty("user.password");
    this.hubUrl = getProperty("hub.url");
  }

  public String getProperty(String propertyName) {
    String propertyValue = System.getenv(propertyName);
    return  propertyValue != null ? propertyValue : properties.getProperty(propertyName);
  }

  public Properties parseProperties() {
    String fileName = env + ".properties";
    try (
        InputStream inputStream = getClass().getClassLoader().getResource(fileName)
        .openStream()
    ) {
      Properties properties = new Properties();
      properties.load(inputStream);
      return properties;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
