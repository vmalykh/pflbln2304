package com.pflb.education.atexample.conditions;

import com.codeborne.selenide.CheckResult;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Driver;
import javax.annotation.Nonnull;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class UserHaveMoneyCondition extends Condition {
  int amountToCheck;

  public UserHaveMoneyCondition(int amountToCheck) {
    super("user have money");
    this.amountToCheck = amountToCheck;
  }

  public UserHaveMoneyCondition() {
    this(0);
  }

  @Nonnull
  @Override
  public CheckResult check(Driver driver, WebElement element) {
    WebElement moneycell = element.findElement(By.xpath("./td[6]"));
    int money = Integer.parseInt(moneycell.getText());
    boolean result = money > amountToCheck;
    return new CheckResult(result, result ? "have money" : "don't have money");
  }
}
