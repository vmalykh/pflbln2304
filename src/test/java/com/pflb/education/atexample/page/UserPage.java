package com.pflb.education.atexample.page;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.pflb.education.atexample.domain.User;
import com.pflb.education.atexample.page.widgets.UsersTable;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserPage extends AbstractPage {
  @FindBy(css = "table tbody tr")
  private List<WebElement> userRows;

  @FindBy(css = "#root table")
  private UsersTable usersTable;

  @FindBy(css = "#root table")
  private WebElement vanillaUsersTable; //user table for vanilla Selenium example


  private static final int FIRST_NAME_COL = 1;
  private static final int LAST_NAME_COL = 2;

  public UserPage() {
    super();
  }

  private List<WebElement> getUserRowCells(int num) {
    WebElement tableRow = userRows.get(num);
    return tableRow.findElements(By.cssSelector("td"));
  }

  public String getUserFirstNameNameByRowNum(int num) {
    List<WebElement> tds = getUserRowCells(num);
    return tds.get(FIRST_NAME_COL).getText();
  }

  public String getUserLastNameNameByRowNum(int num) {
    List<WebElement> tds = getUserRowCells(num);
    return tds.get(LAST_NAME_COL).getText();
  }

  public UsersTable getUsersTable() {
    return usersTable;
  }

  private List<WebElement> findUsersTrs() {
    return vanillaUsersTable.findElements(By.cssSelector("tbody tr"));
  }

  public List<User> parseUsers() {
    List<WebElement> usersTrs = findUsersTrs();
    List<User> userList = new ArrayList<>();
    for (WebElement usersTr : usersTrs) {
      WebElement idColumn = usersTr.findElements(By.cssSelector("td")).get(0);
      WebElement nameColumn = usersTr.findElements(By.cssSelector("td")).get(1);
      Integer id = Integer.parseInt(idColumn.getText());
      User user = new User(id, nameColumn.getText());
      userList.add(user);
    }
    return userList;
  }
}
