package com.pflb.education.atexample.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {

  @FindBy(css = "input[name=email]")
  private SelenideElement loginInput;

  @FindBy(css = "input[name=password]")
  private SelenideElement passwordInput;

  @FindBy(css = "button[type=submit]")
  private SelenideElement submitBtn;

  public LoginPage() {
//    loginInput = driver.findElement(By.cssSelector("input[name=email]"));
//    passwordInput = driver.findElement(By.cssSelector("input[name=password]"));
//    submitBtn = driver.findElement(By.cssSelector("button[type=submit]"));
    super();
  }

  private Alert findAlert() {
    return wait.until(drv -> drv.switchTo().alert());
  }

  public void fillLoginInput(String text) {
    loginInput.clear();
    loginInput.sendKeys(text);
  }

  public void fillPasswordInput(String text) {
    passwordInput.clear();
    passwordInput.sendKeys(text);
  }

  public void submitForm() {
    submitBtn
        .should(Condition.enabled)
        .click();
  }

  public String getAlertText() {
    return findAlert().getText();
  }

  public void dismissAlert() {
    findAlert().dismiss();
  }

  @Deprecated
  public String getAlertTextThenDismiss() {
    return wait.until(drv -> {
      Alert alert = drv.switchTo().alert();
      String text = alert.getText();
      alert.dismiss();
      return text;
    });
  }
}
