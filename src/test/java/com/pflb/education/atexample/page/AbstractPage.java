package com.pflb.education.atexample.page;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage {
  protected WebDriver driver;
  protected WebDriverWait wait;

  public AbstractPage() {
    Selenide.page(this);
  }

}
