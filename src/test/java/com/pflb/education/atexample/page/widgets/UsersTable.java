package com.pflb.education.atexample.page.widgets;

import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;
import java.util.List;
import org.openqa.selenium.support.FindBy;

public class UsersTable extends ElementsContainer {
  @FindBy(css = "thead")
  private SelenideElement tableHeader;

  @FindBy(xpath = ".//tbody")
  private SelenideElement tableBody;

  @FindBy(xpath = ".//tbody/tr")
  private List<UserWidget> users;


  public List<UserWidget> getUsers() {
    return users;
  }

  public static class UserWidget extends ElementsContainer {
    private static final int ID_COLUMN = 1;
    private static final int NAME_COLUMN = 2;

    @FindBy(xpath = "./td[" + ID_COLUMN + "]")
    private SelenideElement id;

    @FindBy(css = "td:nth-child(" + NAME_COLUMN + ")")
    private SelenideElement name;

    public SelenideElement getId() {
      return id;
    }

    public SelenideElement getName() {
      return name;
    }
  }
}
