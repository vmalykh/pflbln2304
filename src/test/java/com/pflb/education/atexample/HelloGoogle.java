package com.pflb.education.atexample;

import static org.openqa.selenium.remote.CapabilityType.BROWSER_NAME;

import com.pflb.education.atexample.config.ApplicationConfig;
import com.pflb.education.atexample.page.LoginPage;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(Lifecycle.PER_CLASS)
public class HelloGoogle {

  private WebDriver driver;
  private ApplicationConfig config;
  private WebDriverWait wait;

  @BeforeAll
  public void configInit() {
    config = new ApplicationConfig();
  }

  @BeforeEach
  public void init() throws MalformedURLException {
    if (config.hubUrl != null) {
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability(BROWSER_NAME, "firefox");
      URL url = URI.create(config.apiUrl).toURL();
      driver = new RemoteWebDriver(url, capabilities);
    } else {
      ChromeOptions chromeOptions = new ChromeOptions();
      if (System.getenv("HEADLESS`") != null) chromeOptions.addArguments("--headless");
      chromeOptions.addArguments("--remote-allow-origins=*");
      driver = new ChromeDriver(chromeOptions);
    }
    wait = new WebDriverWait(driver, Duration.ofSeconds(20));
  }

  @AfterEach
  public void tearDown() {
    driver.quit();
  }

  @Test
  public void loginTest() {
    driver.get(config.baseUrl);

    WebElement loginInput = driver.findElement(By.cssSelector("input[name=email]"));
//    WebElement loginInput = driver.findElement(By.xpath("//input[@name='email']"));
    WebElement passwordInput = driver.findElement(By.cssSelector("input[name=password]"));
    WebElement submitBtn = driver.findElement(By.cssSelector("button[type=submit]"));

    loginInput.sendKeys(config.username);
    passwordInput.sendKeys(config.userPassword);
    submitBtn.click();

//    String alertText = "";
    boolean exitFlag = false;

    String alertText = wait.until(driver -> {
      Alert alert = driver.switchTo().alert();
      String text = alert.getText();
      alert.dismiss();
      return text;
    });

//    while (!exitFlag) {
//      try {
//        Alert alert = driver.switchTo().alert();
//        alertText = alert.getText();
//        alert.dismiss();
//        exitFlag = true;
//        Thread.sleep(100);
//      } catch (Exception ignored) {}
//    }

    Assertions.assertTrue(alertText.contains("Successful"),
        "Alert text doesn't contains info about successful auth");
  }

  @Test
  public void loginTestUsingPo() {
    driver.get(config.baseUrl);
    LoginPage loginPage = new LoginPage();
    loginPage.fillLoginInput(config.username);
    loginPage.fillPasswordInput(config.userPassword);
    loginPage.submitForm();
    String alertText = loginPage.getAlertText();
    Assertions.assertTrue(alertText.contains("Successful"),
        "Alert text doesn't contains info about successful auth");
    loginPage.dismissAlert();
  }
}
