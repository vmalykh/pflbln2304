package com.pflb.education.atexample.cucumber.stepdefinition;

import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.И;

public class LoginStepDefinition {

  @Дано("пользователь заходит на главную страницу")
  public void openPage() {
    System.out.println("open page");
  }

  @И("^вводит логин \"(.+)\"$")
  public void inputLogin(String login) {
    System.out.println(login);
  }

  @И("вводит пароль {string}")
  public void inputPassword(String password) {
    System.out.println(password);
  }
}
