package com.pflb.education.atexample.cucumber;

import com.pflb.education.atexample.config.ApplicationConfig;
import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JunitHooks {
  private RemoteWebDriver driver;
  private ApplicationConfig config;
  private WebDriverWait wait;

  @BeforeAll
  public void configInit() {
    config = new ApplicationConfig();
  }

  @BeforeEach
  public void init() {
    ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.addArguments("--remote-allow-origins=*");
    driver = new ChromeDriver(chromeOptions);
    wait = new WebDriverWait(driver, Duration.ofSeconds(20));
  }

  @AfterEach
  public void tearDown() {
    driver.quit();
  }
}
