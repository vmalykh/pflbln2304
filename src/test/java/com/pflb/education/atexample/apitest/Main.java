package com.pflb.education.atexample.apitest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

import com.pflb.education.atexample.apitest.filters.JwtAuthFilter;
import com.pflb.education.atexample.config.ApplicationConfig;
import com.pflb.education.java.homework.http.dto.Authorization.AuthRequest;
import com.pflb.education.java.homework.http.dto.Authorization.AuthResponse;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Main {

  private static ApplicationConfig config;

  @BeforeAll
  public static void configInit() {
    config = new ApplicationConfig();
    RestAssured.baseURI = config.apiUrl;
    RestAssured.filters(
        new JwtAuthFilter(config.username, config.userPassword),
        new RequestLoggingFilter(),
        new ResponseLoggingFilter());
  }

  @Test
  public void loginTest() {
    AuthRequest authRequest = new AuthRequest(config.username, config.userPassword);

    Response response = given()
          .contentType(ContentType.JSON)
          .body(authRequest)
        .when()
          .post( "/login");

    response.then()
        .statusCode(202)
        .body(not(notNullValue()));

    String string = response.getBody().asString();
//    System.out.println(string);

    AuthResponse authResponse = response.getBody().as(AuthResponse.class);
//    System.out.println(authResponse.accessToken);
  }

  @Test
  public void filterTest() {
    given()
    .when()
        .get("/users");

  }
}
