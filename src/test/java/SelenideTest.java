import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.pflb.education.atexample.conditions.UserHaveMoneyCondition;
import com.pflb.education.atexample.config.ApplicationConfig;
import com.pflb.education.atexample.page.UserPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;

public class SelenideTest {

  private ApplicationConfig config;

  @BeforeEach
  public void setUp() {
    config = new ApplicationConfig();
    Configuration.browser = "chrome";
    Configuration.timeout = 10_000;
    Configuration.baseUrl = config.baseUrl;
  }

  @Test
  public void loginTest() {
    open("/");
    $("input[name=email]").val(config.username);
    $("input[name=password]").val(config.userPassword);
    $("button[type=submit]")
        .should(Condition.enabled)
        .click();

    UserPage userPage = new UserPage();
    userPage.getUsersTable().getUsers().get(1).getSelf()
        .should(new UserHaveMoneyCondition(1000));

    String alertText = Selenide.Wait().until(d -> {
      Alert alert = Selenide.switchTo().alert();
      String text = alert.getText();
      alert.dismiss();
      return text;
    });

    Assertions.assertTrue(alertText.contains("Successful"), "Alert text doesn't contains info about successful auth");
  }
}
